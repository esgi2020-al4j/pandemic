Feature: wallet

  Scenario: empty wallet

    Given an empty wallet
    When I evaluate it in EUR
    Then I should have 0.00 EUR

  Scenario: wallet with single Money

    Given a wallet with 2.00 EUR
    When I evaluate it in EUR
    Then I should have 2.00 EUR

  Scenario: wallet with single Money evaluate in an other

    Given a wallet with 10.00 USD
    And conversion rate USD -> EUR is 0.80
    When I evaluate it in EUR
    Then I should have 8.00 EUR

  Scenario: wallet with a list of monies converted into an other one

    Given a wallet composed by 10.00 USD, 1.50 BTC
    And conversion rate USD -> EUR is 0.80
    And conversion rate BTC -> EUR is 10000.00
    When I evaluate it in EUR
    Then I should have 15008.00 EUR


