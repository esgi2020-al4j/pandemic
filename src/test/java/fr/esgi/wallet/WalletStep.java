package fr.esgi.wallet;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WalletStep {

  private Wallet wallet = new Wallet();
  private Money valuation = Money.ZERO;
  private List<ConversionRate> currentConversionRates = new ArrayList<>();

  @Given("^an empty wallet$")
  public void anEmptyWallet() {
    wallet = new Wallet();
  }

  @Given("^a wallet with (.*)$")
  public void aWalletWithEUR(String money) {
    wallet = new Wallet(toMoney(money));
  }

  @Given("^a wallet composed by (.*)$")
  public void composition(List<String> money) {
    final List<Money> monies = money.stream().map(this::toMoney).collect(Collectors.toList());

    wallet = new Wallet(monies);
  }

  @And("^conversion rate (.*) -> (.*) is (.*)$")
  public void conversionRateUSDEURIs(String from, String to, String rate) {
    currentConversionRates.add(new ConversionRate(from, to, new BigDecimal(rate)));
  }

  @When("^I evaluate it in (.*)$")
  public void iEvaluateItInEUR(String currency) {
    valuation = wallet.evaluateIn(currency, currentConversionRates);
  }

  @Then("^I should have (.*)$")
  public void iShouldHaveEUR(String money) {
    final Money valuation = toMoney(money);
    Assertions.assertThat(valuation).isEqualTo(this.valuation);
  }

  private Money toMoney(final String money) {
    final String[] split = money.split(" ");
    return new Money(new BigDecimal(split[0]), split[1]);
  }
}
