package fr.esgi.wallet;

import java.math.BigDecimal;
import java.util.Objects;

public class ConversionRate {

  private final String from;
  private final String to;
  private final BigDecimal rate;

  public ConversionRate(final String from, final String to, final BigDecimal rate) {
    this.from = from;
    this.to = to;
    this.rate = rate;
  }

  private String from() {
    return from;
  }

  public String to() {
    return to;
  }

  public BigDecimal rate() {
    return rate;
  }

  public boolean isApplicableTo(String currency) {
    return currency.equals(from());
  }

  public Money apply(final Money money) {
    return new Money(money.value().multiply(rate()), to());
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof ConversionRate)) return false;
    final ConversionRate that = (ConversionRate) o;
    return Objects.equals(from, that.from) &&
            Objects.equals(to, that.to) &&
            Objects.equals(rate, that.rate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(from, to, rate);
  }

  @Override
  public String toString() {
    return from + " -> " + to + " : " + rate;
  }
}
