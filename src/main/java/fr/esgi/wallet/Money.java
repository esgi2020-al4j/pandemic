package fr.esgi.wallet;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class Money {

  public static Money ZERO = new Money(BigDecimal.ZERO, "EUR");

  private final BigDecimal value;
  private final String currency;

  public Money(final BigDecimal value, final String currency) {
    this.value = value.setScale(2, RoundingMode.HALF_DOWN);
    this.currency = currency;
  }

  public String currency() {
    return currency;
  }

  public BigDecimal value() {
    return value;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof Money)) return false;
    final Money money = (Money) o;
    return Objects.equals(value, money.value) &&
            Objects.equals(currency, money.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, currency);
  }

  @Override
  public String toString() {
    return value + currency;
  }

  public Money add(final Money money) {
    return new Money(value.add(money.value), money.currency);
  }
}
