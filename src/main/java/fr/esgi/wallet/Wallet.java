package fr.esgi.wallet;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static fr.esgi.wallet.Money.ZERO;

public class Wallet {

  private final List<Money> monies;

  public Wallet(final Money monies) {
    this.monies = Collections.singletonList(monies);
  }

  public Wallet() {
    this(ZERO);
  }

  public Wallet(final List<Money> monies) {
    this.monies = monies;
  }

  private Money convert(final Money money, final ConversionRate conversionRate) {
    if (conversionRate.isApplicableTo(money.currency())) {
      return conversionRate.apply(money);
    }
    return money;
  }

  public Money evaluateIn(final String currency, final List<ConversionRate> conversionRates) {
    final Money result = new Money(BigDecimal.ZERO, currency);
    return monies.stream().reduce(result, (m1, m2) -> {
      if (m1.currency().equals(m2.currency())){
        return m1.add(m2);
      }
      final ConversionRate conversionRate = findApplicableConversionRate(conversionRates, m1, m2);
      return m1.add(convert(m2, conversionRate));
    });

  }

  private ConversionRate findApplicableConversionRate(final List<ConversionRate> conversionRates, final Money m1, final Money m2) {
    return conversionRates.stream().filter(rate -> rate.isApplicableTo(m2.currency()) && rate.to().equals(m1.currency())).findAny().orElseThrow(IllegalArgumentException::new);
  }
}
