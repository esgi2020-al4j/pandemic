# KATA Wallet

Given a Wallet containing Money, build a function that compute the value of wallet in a currency.

Money has a quantity and a currency. The Currency can be for example USD, EUR, GBP, BTC.
